import decimal

class Categoria_anuncio:
    def __init__(self, tipo, valor_bruto, quantidade, valor_liquido=0, cobranca=0):
        self.tipo = tipo
        self.valor_bruto = decimal.Decimal(valor_bruto)
        self.valor_liquido = decimal.Decimal(valor_liquido)
        self.cobranca = decimal.Decimal(cobranca)
        self.quantidade = decimal.Decimal(quantidade)


class Calc_anuncio:
    def __init__(self, tipo, valor_bruto, quantidade, valor_liquido=0, cobranca=0):
        self.tipo = tipo
        self.valor_bruto = decimal.Decimal(valor_bruto)
        self.valor_liquido = decimal.Decimal(valor_liquido)
        self.cobranca = decimal.Decimal(cobranca)
        self.quantidade = decimal.Decimal(quantidade)

    def calc_gratis(self):
        self.valor_liquido = self.valor_bruto
        self.cobranca = 0

    def calc_classico_m(self):
        self.cobranca = (self.valor_bruto * 11) / 100
        self.valor_liquido = decimal.Decimal(self.valor_bruto - self.cobranca)

    def calc_classico_l(self):
        self.cobranca = ((self.valor_bruto * 11) / 100) + 5 * self.quantidade
        self.valor_liquido = decimal.Decimal(self.valor_bruto - self.cobranca)

    def calc_classico(self):
        dic = {
            True: self.calc_classico_l,
            False: self.calc_classico_m
        }
        dic[self.valor_bruto < 120]()

    def calc_premium_m(self):
        self.cobranca = (self.valor_bruto * 16) / 100
        self.valor_liquido = self.valor_bruto - self.cobranca

    def calc_premium_l(self):
        self.cobranca = ((self.valor_bruto * 16) / 100 + 5 * self.quantidade)
        self.valor_liquido = self.valor_bruto - self.cobranca

    def calc_premium(self):
        if self.valor_bruto < 120:
            self.calc_premium_l()
        else:
            self.calc_premium_m()
        # [self.calc_premium_m(),self.calc_premium_l()][self.valor_bruto < 120]

    def calc_type(self):
        types = {
            " Gratis": self.calc_gratis,
            " Classico": self.calc_classico,
            " Premium": self.calc_premium
        }
        if self.tipo == " Clássico":
            types[" Classico"]()
        else:
            types[self.tipo]()

class Calc_gestao:
    def __init__(self,custo1,custo2,tarifa,estornos):
        self.custo1 = custo1
        self.custo2 = custo2
        self.estornos = estornos
        self.tarifa = tarifa
    def custo1_convert(self):
        try:
            return decimal.Decimal(self.custo1)
        except:
            return self.custo1
    def custo2_convert(self):
        try:
            return decimal.Decimal(self.custo2)
        except:
            return self.custo2
    def estornos_convert(self):
        try:
            return decimal.Decimal(self.estornos)
        except:
            return self.estornos
    def tarifa_convert(self):
        try:
            return decimal.Decimal(self.tarifa)
        except:
            return self.tarifa
    def estorno_tarifa(self):
        if (type(self.tarifa_convert()) != str):
            return self.tarifa_convert()
        else:
            return self.estornos_convert()*(-1)
    def custo1_custo2(self):
        if (type(self.custo1_convert())!=str):
            return self.custo1_convert()
        else:
            return self.custo2_convert() * (-1)
    def calc_custo(self):
        return self.custo1_custo2() + self.estorno_tarifa()
#cucucuccu