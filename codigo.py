from anuncio import *
from read import *
from validation import *
from clean import *
from file import *

def open():
    open_file = Openfile("csv files","*.csv")
    file_name=Openfile.take_name(open_file)
    Redcsv_set.red_csv(file_name, validation_1)

def cobranca_planilha(index):
    custo_gestao1 = Get_info_list(index,11,validation_1).get_value()
    custo_gestao1_cleaned = Limpador(custo_gestao1).tira_virgula()
    custo_gestao2 = Get_info_list(index,12,validation_1).get_value()
    custo_gestao2_cleaned = Limpador(custo_gestao2).tira_virgula()
    tarifa_vanda = Get_info_list(index,3,validation_1).get_value()
    tarifa_vanda_cleaned = Limpador(tarifa_vanda).tira_virgula()
    estornos_ebazar = Get_info_list(index,7,validation_1).get_value()
    #print("estornos_ebazar",estornos_ebazar,"\ntarifa_vanda",tarifa_vanda,"\ncusto_gestao2",custo_gestao2,"\ncusto_gestao1",custo_gestao1)
    estornos_ebazar_cleaned = Limpador(estornos_ebazar).tira_virgula()
    print("custo_gestao1_cleaned",custo_gestao1_cleaned)
    print("custo_gestao2_cleaned",custo_gestao2_cleaned)
    print("tarifa_vanda_cleaned",tarifa_vanda_cleaned)
    print("estornos_ebazar_cleaned",estornos_ebazar_cleaned)
    calc_gestao = Calc_gestao(custo_gestao1_cleaned,custo_gestao2_cleaned,tarifa_vanda_cleaned,estornos_ebazar_cleaned)
    #print("calc_gestao",calc_gestao)
    calc_cust = calc_gestao.calc_custo()
    return decimal.Decimal(calc_cust)

def cobranca_calculated(index):
    categoria_anuncio = Categoria_anuncio(validation_1[index][18], validation_1[index][28], validation_1[index][27])
    print("valor_bruto",categoria_anuncio.valor_bruto)
    print("tipo",categoria_anuncio.tipo)
    print("quantidade",categoria_anuncio.quantidade)
    calc_anuncio = Calc_anuncio(categoria_anuncio.tipo, categoria_anuncio.valor_bruto, categoria_anuncio.quantidade)
    calc_anuncio.calc_type()
    return calc_anuncio.cobranca

def validacao(cobranca_planilha,cobranca_calculated):
    validation = Validation(cobranca_planilha,cobranca_calculated).cobranca_validation()
    Validation_enumerate(validation,list_validat).set_bolean()

def start():
    open()
    for x in range (0,len(validation_1)):
        print("\ncust", cobranca_planilha(x))
        print("cobranca", cobranca_calculated(x))
        validacao(cobranca_planilha(x), cobranca_calculated(x))
    for x in range (0,len(validation_1)):
        to_save = Get_info_list(x,19,validation_1).get_value(),list_validat[x]
        FileUtils().writeToFile(to_save,"mercadolivre.gab")
start()

