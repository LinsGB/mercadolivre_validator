class Categoria_anuncio:
    def __init__(self, tipo, valor_bruto, quantidade, valor_liquido=0, cobranca=0):
        self.tipo = tipo
        self.valor_bruto = valor_bruto
        self.valor_liquido = valor_liquido
        self.cobranca = cobranca
        self.quantidade = quantidade


class Calc_anuncio:
    def __init__(self, tipo, valor_bruto, quantidade, valor_liquido=0, cobranca=0):
        self.tipo = tipo
        self.valor_bruto = valor_bruto
        self.valor_liquido = valor_liquido
        self.cobranca = cobranca
        self.quantidade = quantidade

    def calc_gratis(self):
        self.valor_liquido = self.valor_bruto
        self.cobranca = 0

    def calc_classico_m(self):
        self.cobranca = (self.valor_bruto * 11) / 100
        self.valor_liquido = self.valor_bruto - self.cobranca

    def calc_classico_l(self):
        self.cobranca = ((self.valor_bruto * 11) / 100) + 5 * self.quantidade
        self.valor_liquido = self.valor_bruto - self.cobranca

    def calc_classico(self):
        dic = {
            True: self.calc_classico_l,
            False: self.calc_classico_m
        }
        dic[self.valor_bruto < 120]()

    def calc_premium_m(self):
        self.cobranca = (self.valor_bruto * 16) / 100
        self.valor_liquido = self.valor_bruto - self.cobranca
        print("\nOK\n")

    def calc_premium_l(self):
        self.cobranca = ((self.valor_bruto * 16) / 100 + 5 * self.quantidade)
        self.valor_liquido = self.valor_bruto - self.cobranca
        print("\nOK\n")

    def calc_premium(self):
        if self.valor_bruto < 120:
            self.calc_premium_l()
        else:
            self.calc_premium_m()
        # [self.calc_premium_m(),self.calc_premium_l()][self.valor_bruto < 120]

    def calc_type(self):
        types = {
            "gratis": self.calc_gratis,
            "classico": self.calc_classico,
            "premium": self.calc_premium
        }
        types[self.tipo]()


x = Categoria_anuncio("premium", 120, 3)
y = Calc_anuncio(x.tipo,x.valor_bruto,x.quantidade)
y.calc_type()
print(y.cobranca)



