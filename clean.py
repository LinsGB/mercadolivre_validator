import unicodedata
class Limpador:
    def __init__(self,string):
        self.string = string
    def tira_virgula(self):
        return self.string.replace(",",".")
    def tira_acento(self):
        string_nova = ''.join(ch for ch in unicodedata.normalize('NFKD', self.string)if not unicodedata.combining(ch))
        return string_nova